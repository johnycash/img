# THIS IS IMG

Basically if you'd like to learn k8s on your own ProxMox

## Where I've copied things from

* check this one out: https://github.com/chriswayg/packer-proxmox-templates
* also watch this: https://www.youtube.com/watch?v=8qwnXd1yRK4

## Requirements

* You need to install everything from mentioned repo except Ansible
* You may also need sshpass and OpenSSH client
* On Windows 10 WLS Ubuntu, mkpasswd is missing - install whois to provide it

### Why no Ansible?

Because for so simple image this is an overkill. If you have already some playbooks use them, I'm not an enemy of Ansible, I'm just a friend of simplicity.

## Configure

It's a little different from original, as we connect to remote ProxMox machine

## Usage

* Clone
* Copy build.cfg.dist to build.cfg
* Edit build.cfg according to your needs
* Run: ./build.sh proxmox

Yu can use CloudInit to initialize initial cfg of the machine.
